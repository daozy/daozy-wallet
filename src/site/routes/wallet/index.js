const fs = require("fs")
const url = require('url');
const router = require('express').Router();
const log = require("../log").logger();
const DaozyUtils = require("../daozy_utils");

const daozy_utils = new DaozyUtils();

const module_name = 'wallet';
const module_ch_name = 'Daozy 钱包';
const page_prefix = module_name + '/';

router.get('/', function(req, res, next) {
    log.error('xxxxxxxxxxxxxxxxxxxxxxx');
    log.info(req.originalUrl);

    var parameters = {
        title: module_ch_name,
        username: 'test',
        alias: 'test',
        role: 'user'
    };

    res.render(page_prefix + "index", parameters);
});

router.get('/:page', function(req, res, next) {
    log.error('dddddddddddddddddd');

    log.info(req.originalUrl);

    var parameters = {
        title: module_ch_name,
        username: 'test',
        alias: 'test',
        role: 'user'
    };

    var page = req.params['page'];
    res.render(page_prefix + page, parameters);
});

router.post('/', function(req, res, next) {
    try {
        var action = req.body.action;
    } catch (err) {
        log.error(err);
        daozy_utils.send_warn_resp(res, 'parameter action not exist');
        return;
    }

    log.info("==============req.url = %s, action = %s===============", req.originalUrl, action);
    var data = daozy_utils.parser_para(res, req.body.data);

    // 构造数据进行回复
    if (action == 'event_1') {
        daozy_utils.send_data_resp(res, { para1: "xxx", para2: "xxx" });
    } else if (action == 'event_1') {
        daozy_utils.send_data_resp(res, { para1: "xxx", para2: "xxx" });
    }
});

module.exports = router;
